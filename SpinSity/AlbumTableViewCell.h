//
//  AlbumTableViewCell.h
//  SpinSity
//
//  Created by Илья on 09.05.17.
//  Copyright © 2017 И.И.В. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AlbumTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *albumTitleLabel;

@property (weak, nonatomic) IBOutlet UILabel *albumSummaryLabel;

@property (weak, nonatomic) IBOutlet UILabel *priceLabel;

@end
