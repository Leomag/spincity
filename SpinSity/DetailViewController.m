//
//  DetailViewController.m
//  SpinSity
//
//  Created by Илья on 09.05.17.
//  Copyright © 2017 И.И.В. All rights reserved.
//

#import "DetailViewController.h"
#import "Album.h"

@interface DetailViewController ()

@end

@implementation DetailViewController

- (void)configureView {
    // Update the user interface for the detail item.
    if (self.detailItem) {
        
        self.albumTitleLabel.text = self.detailItem.title;
        self.priceLabel.text = [NSString stringWithFormat:@"$%01.2f", self.detailItem.price];
        self.artistLabel.text = self.detailItem.artist;
        self.locationLabel.text = self.detailItem.locationInStore;
        self.descriptionTextView.text = self.detailItem.summary;
    }
}


- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    [self configureView];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark - Managing the detail item

- (void)setDetailItem:(NSDate *)newDetailItem {
    if (_detailItem != newDetailItem) {
        _detailItem = newDetailItem;
        
        // Update the view.
        [self configureView];
    }
}


@end
