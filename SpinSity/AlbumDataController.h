//
//  AlbumDataController.h
//  SpinSity
//
//  Created by Илья on 09.05.17.
//  Copyright © 2017 И.И.В. All rights reserved.
//


#import <Foundation/Foundation.h>

@class Album;

@interface AlbumDataController : NSObject

-(NSUInteger)albumCount;
-(Album *)albumAtIndex:(NSUInteger)index;

-(void)addAlbumWithTitle:(NSString *)title artist:(NSString *)artist summary:(NSString *)summary price:(float)price locationInStore:(NSString *)locationInStore;

@end
