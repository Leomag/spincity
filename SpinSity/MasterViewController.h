//
//  MasterViewController.h
//  SpinSity
//
//  Created by Илья on 09.05.17.
//  Copyright © 2017 И.И.В. All rights reserved.
//

#import <UIKit/UIKit.h>

@class DetailViewController;

@interface MasterViewController : UITableViewController



@property (strong, nonatomic) DetailViewController *detailViewController;


@end

