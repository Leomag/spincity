//
//  Album.m
//  SpinSity
//
//  Created by Илья on 09.05.17.
//  Copyright © 2017 И.И.В. All rights reserved.
//

#import "Album.h"

@implementation Album


-(id)initWithTitle:(NSString *)title artist:(NSString *)artist summary:(NSString *)summary price:(float)price locationInStore:(NSString *)locationInStore {
    
    self = [super init]; if (self) {
        _title = title;
        _artist = artist;
        _summary = summary;
        _price = price;
        _locationInStore = locationInStore;
        
        return self;
    }
    
    return nil;
}


@end
